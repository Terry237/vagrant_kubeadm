def is_palindrome(s):
    # Normalize the string: remove non-alphanumeric characters and convert to lowercase
    normalized_string = ''.join(char.lower() for char in s if char.isalnum())

    # Check if the string is equal to its reverse
    return normalized_string == normalized_string[::-1]

